let btn = document.querySelector('.button');
let img = document.querySelector('img');
let msg = document.querySelector('.message');

btn.addEventListener('click', (e) => {
    img.style.animation = "fade ease-in-out 1.2s normal forwards";
    msg.style.animation = "fade ease-in-out .6s reverse forwards";
})